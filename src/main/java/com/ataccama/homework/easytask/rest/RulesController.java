package com.ataccama.homework.easytask.rest;

import com.ataccama.homework.easytask.exceptions.IllegalRuleModificationException;
import com.ataccama.homework.easytask.exceptions.NoSuchRuleException;
import com.ataccama.homework.easytask.records.Filter;
import com.ataccama.homework.easytask.records.CsvRecordsProcessor;
import com.ataccama.homework.easytask.rules.Rule;
import com.ataccama.homework.easytask.rules.RuleRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RulesController {

    // a repo taking care of the rules
    private final RuleRepository repo;
    private final CsvRecordsProcessor evaluator;

    /**
     * creates a new instance of the controller with access to specific repository
     * @param repository the repository, that should be used by the new instance
     */
    public RulesController(RuleRepository repository){
        this.repo = repository;
        this.evaluator = new CsvRecordsProcessor();
    }

    /**
     * returns all rules stored in the repository
     * @return list of all rules present in the repository
     */
    @GetMapping("/rules")
    @ResponseBody
    public List<Rule> getRules(){
        return repo.findAll();                                      // get all the rules from the repo
    }

    /**
     * returns a single rule from the repository by id
     * @param id id of the rule we want to retrieve
     * @return rule corresponding to given id, if there is any
     */
    @RequestMapping(path="/rules/{id}", method= RequestMethod.GET)
    @ResponseBody
    public Rule getRule(
            @PathVariable("id") int id){
        return repo.findById(id)                                    // if a rule with this id exists return it
                .orElseThrow(() -> new NoSuchRuleException(id));    // else throw exception
    }

    /**
     * stores new rule in the repository
     * @param newRule the new rule to be stored
     * @return information about the rule we just stored
     */
    @PostMapping("/rules")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Rule postRule(@RequestBody Rule newRule){
        return repo.save(newRule);                                  // create new rule in the repo
    }

    /**
     * deletes a rule from repository by id
     * @param id id of the rule we want to remove
     */
    @DeleteMapping("/rules/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteRule(@PathVariable("id") int id){
        try {
            repo.deleteById(id);                                        // remove the rule by id
        }catch (EmptyResultDataAccessException erdae){
            throw new NoSuchRuleException(id);
        }
    }

    /**
     * updates rule in repository with new values.
     * rule is found by id
     *
     * @param newRule rule description containing new values
     * @param id id of the rule we want to update
     * @return information about the updated rule
     */
    @PutMapping("/rules/{id}")
    @ResponseBody
    public Rule putRule(@RequestBody Rule newRule, @PathVariable("id") int id){
        return repo.findById(id)                                    // find corresponding rule in the repo
                .map(rule -> updateValues(rule,newRule))            // if it exists update its values
                .orElseThrow(() -> new NoSuchRuleException(id));    // otherwise throw an exception
    }

    /**
     * modifies existing rule with new values, if the user is allowed to modify these values
     * @param rule the rule we want to modify
     * @param newRule rule containing the modifications we want to do
     * @return modified rule
     * @throws IllegalRuleModificationException thrown upon request to modify non-modifiable fields of the rule
     */
    private Rule updateValues(Rule rule, Rule newRule) throws IllegalRuleModificationException {
        if(newRule.isValidModificationOf(rule)){
            rule.applyModification(newRule);                        // modify the rule
            return repo.save(rule);                                 // persist the modification
        }else {
            throw new IllegalRuleModificationException();           // if the modification is invalid, throw exception
        }
    }

    @PostMapping("/rules/{id}/eval")
    @ResponseBody
    public String evaluateRule(
            @RequestBody String csv,
            @PathVariable("id") int id,
            @RequestParam(value = "filter",required = false) String filter
    ){
        Filter f = new Filter(filter);                    // create corresponding filter for results
        Rule rule = this.getRule(id);                     // find the rule we want to use
        return evaluator.evaluateSingleRule(csv,rule,f);  // and evaluate the rule on every line of the csv
    }

    @PostMapping("/eval")
    public String evaluateAllRules(
            @RequestBody String csv,
            @RequestParam(value = "filter", required = false) String filter
    ){
        Filter f = new Filter(filter);                    // create corresponding filter for results
        List<Rule> rules = this.getRules();               // get all rules
        return evaluator.evaluateMultipleRules(csv,rules,f);    // evaluate all enabled rules on every line of the csv
    }

}
