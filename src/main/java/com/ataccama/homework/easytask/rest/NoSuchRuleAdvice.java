package com.ataccama.homework.easytask.rest;


import com.ataccama.homework.easytask.exceptions.NoSuchRuleException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class NoSuchRuleAdvice {

    @ResponseBody
    @ExceptionHandler(NoSuchRuleException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String noSuchRuleHandler(NoSuchRuleException exception){
        return exception.getMessage();
    }
}
