package com.ataccama.homework.easytask.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ArrayOutOfBoundsAdvice {

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ArrayIndexOutOfBoundsException.class)
    public String arrayOutOfBoundsHandler(ArrayIndexOutOfBoundsException ex){
        return "Not enough columns";
    }
}
