package com.ataccama.homework.easytask.rest;

import com.ataccama.homework.easytask.exceptions.IllegalRuleModificationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class IllegalModificationAdvice {

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(com.ataccama.homework.easytask.exceptions.IllegalRuleModificationException.class)
    public String illegalModificationHandler(IllegalRuleModificationException exception){
        return exception.getMessage();
    }
}
