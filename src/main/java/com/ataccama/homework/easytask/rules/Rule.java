package com.ataccama.homework.easytask.rules;

import com.ataccama.homework.easytask.exceptions.IllegalRuleModificationException;
import com.ataccama.homework.easytask.rules.arguments.*;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

/**
 * representation of single rule for data filtering
 */
@Data
@Entity
@JsonPropertyOrder({ "id", "name", "enabled", "op", "arg1", "arg2" })
@SequenceGenerator(name="seq", initialValue=1)
public class Rule {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    @JsonIgnore
    private int id;                             // rule id - autogenerated
    private String name;                        // rule name
    private boolean enabled;                    // rule is enabled/disabled
    private String op;                          // rule operation
    @ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.EAGER)
    private RuleArgument arg1;                  // value of the first arg
    @ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.EAGER)
    private RuleArgument arg2;                  // value of the second arg

    public Rule(){}

    @JsonCreator
    public Rule(
        @JsonProperty("name") String name,
        @JsonProperty("enabled") boolean enabled,
        @JsonProperty("op") String op,
        @JsonProperty("arg1") RuleArgument arg1,
        @JsonProperty("arg2") RuleArgument arg2
    ){
        this.name = name;                       // store name
        this.enabled = enabled;                 // enable/disable
        this.op = op;                           // store op
        this.arg1 = arg1;                       // store arg1
        this.arg2 = arg2;                       // store arg2
    }


    @JsonProperty("id")
    public int getId(){ return id; }

    @JsonIgnore
    public int setId(){ throw new IllegalRuleModificationException(); }

    public String getName() {
        return name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public RuleArgument getArg1(){
        return arg1;
    }

    public RuleArgument getArg2(){
        return arg2;
    }

    public String getOp(){
        return op;
    }

    /**
     * verify that this rule is a valid modification of some other rule
     * @param otherRule a rule which we inted to modify by this
     * @return true, if this rule differs from the other rule in name, id, and state of enabled
     */
    public boolean isValidModificationOf(Rule otherRule){
        if(!this.op.equals(otherRule.op)){
            return false;                               // we are not allowed to modify the operation
        }else return (this.arg1.equals(otherRule.arg1)) && (this.arg2.equals(otherRule.arg2));
    }

    /**
     * updates modifiable fields of this rule
     * @param newRule a representation of the modification
     */
    public void applyModification(Rule newRule){
        this.name = newRule.getName();
        this.enabled = newRule.isEnabled();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rule)) return false;
        Rule rule = (Rule) o;
        return isEnabled() == rule.isEnabled() &&
                Objects.equals(getName(), rule.getName()) &&
                Objects.equals(getOp(), rule.getOp()) &&
                Objects.equals(getArg1(), rule.getArg1()) &&
                Objects.equals(getArg2(), rule.getArg2());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), isEnabled(), getOp(), getArg1(), getArg2());
    }
}
