package com.ataccama.homework.easytask.rules;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * interface of jpa repository for rules
 */
public interface RuleRepository extends JpaRepository<Rule, Integer> {
}
