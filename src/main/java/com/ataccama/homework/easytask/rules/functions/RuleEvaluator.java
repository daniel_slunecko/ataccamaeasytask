package com.ataccama.homework.easytask.rules.functions;

import com.ataccama.homework.easytask.exceptions.UndefinedOperationException;

/**
 * class for determining if two arguments are in given relation
 * @param <T> type of the compared argument values
 */
public class RuleEvaluator<T extends Comparable<T>> {

    public RuleEvaluator(){}

    /**
     * confirms that the arguments are in given relation
     * @param arg1 value of the first argument
     * @param op operator expressing the relation we expect to hold between the arguments
     * @param arg2 value of the second argument
     * @return true if the arguments are in relation described by the operator, else false
     * @throws UnsupportedOperationException if the operator is not defined
     */
    public Boolean evaluate(T arg1, String op, T arg2) throws UndefinedOperationException {
        switch(op){
            case "<":
                return (arg1.compareTo(arg2)) < 0;
            case ">":
                return (arg1.compareTo(arg2)) > 0;
            case "<=":
                return (arg1.compareTo(arg2)) <= 0;
            case ">=":
                return (arg1.compareTo(arg2)) >= 0;
            case "=":
                return (arg1.compareTo(arg2)) == 0;
            case "!=":
                return (arg1.compareTo(arg2)) != 0;
            default:
                throw new UndefinedOperationException("the operation: "+ op+ " is not supported");
        }
    }
}
