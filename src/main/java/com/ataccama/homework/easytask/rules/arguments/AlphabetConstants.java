package com.ataccama.homework.easytask.rules.arguments;

class AlphabetConstants {
    static final int ORD_OF_AT_SIGN = 64;       // ord value of the @, which is just before A in ASCII
    static final int ALPHABET_LENGTH = 26;     // length of the english alphabet (used for column descriptors)
}
