package com.ataccama.homework.easytask.exceptions;

public class IllegalRuleModificationException extends RuntimeException {
    public IllegalRuleModificationException(){
        super("Illegal modification request. User is allowed to modify only name and/or state of enabled/disabled.");
    }
}
