package com.ataccama.homework.easytask.exceptions;

/**
 *  Exception indicating invalid type of argument data
 */
public class InvalidArgumentValue extends RuntimeException {
    public InvalidArgumentValue(String message){
        super(message);
    }
}
