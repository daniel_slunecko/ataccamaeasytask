package com.ataccama.homework.easytask.exceptions;

/**
 * Exception indicating invalid argument for rule creation
 */
public class InvalidColumnDescriptorException extends RuntimeException {
    public InvalidColumnDescriptorException(String message){
        super(message);
    }
}
