package com.ataccama.homework.easytask.exceptions;

/**
 * Exception indicating unknown operation symbol passed to rule
 */
public class UndefinedOperationException extends RuntimeException {
    public UndefinedOperationException(String message){
        super(message);
    }
}
