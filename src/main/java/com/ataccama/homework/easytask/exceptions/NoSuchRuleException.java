package com.ataccama.homework.easytask.exceptions;

/**
 * Exception indicating attempt to access non-existing rule in a repository
 */
public class NoSuchRuleException extends RuntimeException{
    public NoSuchRuleException(String message){
        super(message);
    }

    public NoSuchRuleException(int id){
        this("A Rule with id: "+ id + " doesn't exist");
    }
}
