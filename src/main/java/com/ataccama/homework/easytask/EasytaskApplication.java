package com.ataccama.homework.easytask;

import com.ataccama.homework.easytask.rules.functions.RuleEvaluator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasytaskApplication {
    public static final RuleEvaluator<Integer> RULE_EVALUATOR = new RuleEvaluator<>();

    public static void main(String[] args) {
        SpringApplication.run(EasytaskApplication.class, args);
    }

}

