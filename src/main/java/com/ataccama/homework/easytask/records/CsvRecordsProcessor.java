package com.ataccama.homework.easytask.records;

import com.ataccama.homework.easytask.rules.Rule;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * a class providing methods for rules application onto records in a form of csv
 */
public class CsvRecordsProcessor {
    static final String COMMA = ",";
    private static final String EOL = System.lineSeparator();

    /**
     * applies given rule to all lines in an input csv and returns list of results
     * @param csv csv data in a single string
     * @param rule the rule we want to apply on the csv
     * @param f class for filtering only the desired results
     * @return csv obtained by application of the rule on lines of the original csv
     */
    public String evaluateSingleRule(String csv, Rule rule,Filter f){
        BufferedReader reader = new BufferedReader(new StringReader(csv));
        Stream<Result> results =  reader.lines()            // get lines of the csv file
                .map(line -> new Record(line, COMMA))       // create record for each line
                .map(record -> record.apply(rule))          // apply the rule to each of the records
                .filter(f::apply);                          // filter only the required results
        return mkResultCSV(results);                        // return a csv of the results
    }

    /**
     * applies a list of rules to each line of a string representing csv
     * @param csv csv data in a single string
     * @param rules list of rules that should be applied to each line of the csv
     * @param f class for filtering only the desired results
     * @return csv obtained by application of the rules on lines of the original csv
     */
    public String evaluateMultipleRules(String csv, List<Rule> rules, Filter f){
        BufferedReader reader = new BufferedReader(new StringReader(csv));
        Stream<FullResult> results =  reader.lines()        // get lines of the csv file
                .map(line -> new Record(line, COMMA))       // create record for each line
                .map(record -> record.apply(rules))         // apply all the rules to each of the records
                .filter(f::apply);                          // filter only the desired results
        return mkFullResultCSV(results);
    }

    private String mkResultCSV(Stream<Result> stream){
        List<String> list =                                 // list of csv lines, get as follows
                stream.map(Result::toCSVLine)               // transform result to one line of csv
                        .collect(Collectors.toList());      // collect results
        return String.join(EOL,list);                     // transform all the lines into one csv
    }

    private String mkFullResultCSV(Stream<FullResult> stream){
        List<String> list =                                 // list of csv lines, get as follows
                stream.map(FullResult::toCSVLine)           // transform result to one line of csv
                        .collect(Collectors.toList());      // collect results
        return String.join(EOL,list);                     // transform all the lines into one csv
    }
}
