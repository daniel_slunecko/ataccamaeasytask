package com.ataccama.homework.easytask.records;


/**
 * class for filtering results of rule applications
 */
public class Filter {
    private String filter;

    public Filter(String filter){
        this.filter = filter;
    }

    public boolean apply(Result result){
        if(filter == null){
            return true;
        }else if(filter.equals(result.getResult())){
            return true;
        }else {
            return false;
        }
    }
}
