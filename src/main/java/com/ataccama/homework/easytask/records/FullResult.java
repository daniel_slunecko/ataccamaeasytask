package com.ataccama.homework.easytask.records;

import com.ataccama.homework.easytask.rules.Rule;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static com.ataccama.homework.easytask.records.CsvRecordsProcessor.COMMA;

/**
 * represents result of application of multiple rules onto a record
 */
public class FullResult extends Result {
    public static final String PLUS = "+";
    private String[] results;

    public FullResult(Integer[] values, boolean isOk, Boolean[] results, Rule[] rules) {
        super(values, isOk);
        this.results =
            IntStream.range(0,results.length)
            .filter(i -> !results[i])
            .map(i -> rules[i].getId())
                    .mapToObj(i -> Integer.valueOf(i).toString())
                    .toArray(String[]::new);
    }

    /**
     * transforms the full result of applying list of rules onto a record into a single line of csv
     * @return line of the csv representing the result
     */
    public String toCSVLine() {
        List<String> lineElements = getValues();        // get the values as a list of strings
        lineElements.add(result);                       // append the result to it

        if(result.equals(FAIL)){
            List<String> indices = resultsToList();     // get ids of the failed tests
            String failedIndices = String.join(PLUS,indices); // format the failed indices into a string
            lineElements.add(failedIndices);            // append the indices of the failed tests to the end
        }
        return String.join(COMMA,lineElements);         // transform the data into a csv line
    }

    private List<String> resultsToList(){
        return Arrays.asList(results);                  // create a list from the array
    }

    public String getResult() {
        return result;
    }
}
