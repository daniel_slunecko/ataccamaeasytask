package com.ataccama.homework.easytask.records;

import com.ataccama.homework.easytask.exceptions.InvalidArgumentValue;
import com.ataccama.homework.easytask.rules.Rule;
import com.ataccama.homework.easytask.rules.arguments.RuleArgument;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.ataccama.homework.easytask.EasytaskApplication.RULE_EVALUATOR;

/**
 * Representation of data input.
 * A method for rule application is provided.
 */
public class Record {
    private Integer[] values;

    public Record(){}

    public Record(Integer[] values){
        this.values = values;
    }

    public Record(String[] data){
        this.values = new Integer[data.length];
        for(int i = 0; i < data.length;i++){
            this.values[i] = Integer.valueOf(data[i]);
        }
    }

    public Record(String dataLine, String separator){
        this(dataLine.split(separator));
    }

    public List<String> getValues(){
        return Arrays.stream(values).map(Objects::toString).collect(Collectors.toList());
    }

    /**
     * apply a rule to this record
     * @param r the rule that will be applied
     * @return result obtained by applying the rule on this record
     */
    public Result apply(Rule r) throws ArrayIndexOutOfBoundsException,InvalidArgumentValue{
        boolean isOk = applySingleRule(r);                                    // apply the rule to this record
        return new Result(values,isOk);                                       // return the result
    }

    /**
     * apply all rules from a list to this record
     * @param rules list of the rules we want to apply
     * @return result obtained by application of the rules on this record
     */
    public FullResult apply(List<Rule> rules)throws ArrayIndexOutOfBoundsException,InvalidArgumentValue{

        Boolean[] areOK = rules.stream()                                      // create an array for results from stream of rules
                .filter(Rule::isEnabled)                                      // filter only enabled rules
                .map(this::applySingleRule)                                   // apply each of them to this record
                .toArray(Boolean[]::new);                                     // and store the results to an array
        boolean result =
                Arrays.stream(areOK)                                          // determine the final result
                        .reduce((b1,b2) -> b1 && b2).orElse(true);
        return new FullResult(values,result,areOK,rules.toArray(new Rule[0]));
    }

    private boolean applySingleRule(Rule r) throws ArrayIndexOutOfBoundsException,InvalidArgumentValue{
        int arg1 = getArgumentValue(r.getArg1());                              // get the first arguments value
        int arg2 = getArgumentValue(r.getArg2());                              // get the value of the second argument
        return RULE_EVALUATOR.evaluate(arg1,r.getOp(),arg2);                   // apply the rule on the arguments
    }

    private int getArgumentValue(RuleArgument arg) throws ArrayIndexOutOfBoundsException,InvalidArgumentValue{
        if(arg.isColumnDescriptor()){                                          // if the argument is column description
            return values[arg.columnToIndex()];                                // take the arg value from the column in this record
        } else if(arg.isValueDescriptor()){                                    // else if the argument is value descriptor
            return arg.getValue();                                             // and take its value directly
        }else{
            throw new InvalidArgumentValue("A column description or value is expected, but neither was found.");
        }
    }
}