package com.ataccama.homework.easytask.records;

import java.util.List;

import static com.ataccama.homework.easytask.records.CsvRecordsProcessor.COMMA;

/**
 * represents result of application of single rule onto a record
 */
public class Result extends Record{
    private static final String OK = "OK";
    static final String FAIL = "FAIL";
    protected String result;

    public Result(Integer[] values, boolean isOk) {
        super(values);
        if(isOk){
            this.result = OK;
        }else{
            this.result = FAIL;
        }
    }

    /**
     * converts the result into one line of csv
     * @return a string representing a single line of csv
     */
    public String toCSVLine() {
        List<String> lineElements = getValues();        // get the values as a list of strings
        lineElements.add(result);                       // append the result to it
        return String.join(COMMA,lineElements);         // make string from the list using comma as separator
    }

    public String getResult() {
        return result;
    }
}
