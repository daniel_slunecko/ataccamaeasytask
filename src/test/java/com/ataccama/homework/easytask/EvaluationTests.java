package com.ataccama.homework.easytask;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
@SpringBootTest
public class EvaluationTests extends AbstractRestTest {

    private String jsonAlessB;
    private String jsonValLeqVal;
    private String jsonCNot3;
    private String jsonAnotA;
    private String json7neqD;

    private String jsonAlessBDissabled;
    private String jsonValLeqValDissabled;

    private String sufficientData ;
    private String insufficientData1;

    private String insufficientData3;

    private String result1;
    private String result2;
    private String result3;
    private String failure;
    private String ruleDoesntExist;

    private String result4;
    private String result5;
    private String result6;
    private String result7;


    @Before
    public void setUp(){
        super.setUp();
        jsonAlessB = "{\"name\":\"AlessB\",\"enabled\":true,\"op\":\"<\",\"arg1\":{\"column\":\"A\"},\"arg2\":{\"column\":\"B\"}}";
        jsonValLeqVal = "{\"name\":\"ValLeqVal\",\"enabled\":true,\"op\":\"<=\",\"arg1\":{\"value\":3},\"arg2\":{\"value\":7}}";
        jsonCNot3 = "{\"name\":\"Cnot3\",\"enabled\":true,\"op\":\"!=\",\"arg1\":{\"column\":\"C\"},\"arg2\":{\"value\":\"3\"}}";
        jsonAnotA = "{\"name\":\"AnotA\",\"enabled\":true,\"op\":\"!=\",\"arg1\":{\"column\":\"A\"},\"arg2\":{\"column\":\"A\"}}";
        json7neqD = "{\"name\":\"7neqD\",\"enabled\":true,\"op\":\"!=\",\"arg1\":{\"value\":7},\"arg2\":{\"column\":\"D\"}}";

        jsonAlessBDissabled = "{\"name\":\"AlessB\",\"enabled\":false,\"op\":\"<\",\"arg1\":{\"column\":\"A\"},\"arg2\":{\"column\":\"B\"}}";
        jsonValLeqValDissabled = "{\"name\":\"ValLeqVal\",\"enabled\":false,\"op\":\"<=\",\"arg1\":{\"value\":3},\"arg2\":{\"value\":7}}";

        sufficientData = "1,1,1,1,1,1\n2,3,4,5,6,7\n7,6,5,4,3,2\n5,2,3,7,1,1\n5,2,3,0,0,0\n5,6,3,7,0,0\n5,6,2,0,7,7";
        insufficientData1 = "1,1,1,1,1,1\n2,3,4,5,6,7\n7,6,5,4,3,2\n5,2,3,7,1,1\n5,2,3,0,0,0\n5,6,3,7,0,0\n5,6,2";

        insufficientData3 = "1";

        result1 = "1,1,1,1,1,1,FAIL\n2,3,4,5,6,7,OK\n7,6,5,4,3,2,FAIL\n5,2,3,7,1,1,FAIL\n5,2,3,0,0,0,FAIL\n5,6,3,7,0,0,OK\n5,6,2,0,7,7,OK";
        result2 = "2,3,4,5,6,7,OK\n5,6,3,7,0,0,OK\n5,6,2,0,7,7,OK";
        result3 = "1,1,1,1,1,1,FAIL\n7,6,5,4,3,2,FAIL\n5,2,3,7,1,1,FAIL\n5,2,3,0,0,0,FAIL";
        failure = "Not enough columns";
        ruleDoesntExist = "A Rule with id: 42 doesn't exist";

        result4 = "1,1,1,1,1,1,OK\n2,3,4,5,6,7,OK\n7,6,5,4,3,2,OK\n5,2,3,7,1,1,OK\n5,2,3,0,0,0,OK\n5,6,3,7,0,0,OK\n5,6,2,0,7,7,OK";
        result5 = "1,1,1,1,1,1,FAIL,6\n2,3,4,5,6,7,OK\n7,6,5,4,3,2,FAIL,6\n5,2,3,7,1,1,FAIL,6+8+10\n5,2,3,0,0,0,FAIL,6+8\n5,6,3,7,0,0,FAIL,8+10\n5,6,2,0,7,7,OK";
        result6 = "2,3,4,5,6,7,OK\n5,6,2,0,7,7,OK";
        result7 = "1,1,1,1,1,1,FAIL,6\n7,6,5,4,3,2,FAIL,6\n5,2,3,7,1,1,FAIL,6+8+10\n5,2,3,0,0,0,FAIL,6+8\n5,6,3,7,0,0,FAIL,8+10";
    }

    // test single rule evaluation
    // test evaluation of non-existent rule on sufficient data
    @Test
    public void a_evalSingleNonExistentRuleOnSufficientData() throws Exception {
        try {
            fillDB();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String uri = "/rules/42/eval";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.TEXT_PLAIN)
                .content(sufficientData)).andReturn();

        int status = result.getResponse().getStatus();
        String message = result.getResponse().getContentAsString();
        assertEquals(404,status);
        assertEquals(ruleDoesntExist,message);
    }
    // test evaluation of existing rule on sufficient data
    @Test
    public void a_evalSingleRuleOnSufficientData() throws Exception {
        String uri = "/rules/1/eval";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.TEXT_PLAIN)
                .content(sufficientData)).andReturn();

        int status = result.getResponse().getStatus();
        String csv = result.getResponse().getContentAsString();
        assertEquals(200,status);
        assertEquals(result1,csv);
    }
    // test evaluation of existing rule on sufficient data - filter OK
    @Test
    public void a_evalSingleRuleOnSufficientDataFilterOK() throws Exception {
        String uri = "/rules/1/eval?filter=OK";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.TEXT_PLAIN)
                .content(sufficientData)).andReturn();

        int status = result.getResponse().getStatus();
        String csv = result.getResponse().getContentAsString();
        assertEquals(200,status);
        assertEquals(result2,csv);
    }
    // test evaluation of existing rule on sufficient data - filter FAIL
    @Test
    public void a_evalSingleRuleOnSufficientDataFilterFAIL() throws Exception {
        String uri = "/rules/1/eval?filter=FAIL";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.TEXT_PLAIN)
                .content(sufficientData)).andReturn();

        int status = result.getResponse().getStatus();
        String csv = result.getResponse().getContentAsString();
        assertEquals(200,status);
        assertEquals(result3,csv);
    }
    // test evaluation of existing rule on insufficient data
    @Test
    public void a_evalSingleRuleOnInsufficientData() throws Exception {
        String uri = "/rules/1/eval";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.TEXT_PLAIN)
                .content(insufficientData3)).andReturn();

        int status = result.getResponse().getStatus();
        String message = result.getResponse().getContentAsString();
        assertEquals(400,status);
        assertEquals(failure,message);
    }


    // test all enabled rules evaluation
    // test evalution with empty database
    @Test
    public void b_evalMultipleRulesOnEmptyDB() throws Exception {
        String uri = "/eval";
        removeAllFromDB();

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.TEXT_PLAIN)
                .content(sufficientData)).andReturn();

        int status = result.getResponse().getStatus();
        String csv = result.getResponse().getContentAsString();
        assertEquals(200,status);
        assertEquals(result4,csv);
    }
    // test evalution with all rules disabled
    @Test
    public void c_evalMultipleRules_DisabledRulesOnly() throws Exception{
        String uri = "/eval";
        postPart1();
        disable();

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.TEXT_PLAIN)
                .content(sufficientData)).andReturn();

        int status = result.getResponse().getStatus();
        String csv = result.getResponse().getContentAsString();
        assertEquals(200,status);
        assertEquals(result4,csv);
    }
    // test multiple rules on sufficient data
    @Test
    public void d_evalMultipleRules() throws Exception{
        String uri = "/eval";
        enable();
        postPart2();
        removeContradiction();

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.TEXT_PLAIN)
                .content(sufficientData)).andReturn();

        int status = result.getResponse().getStatus();
        String csv = result.getResponse().getContentAsString();
        assertEquals(200,status);
        assertEquals(result5,csv);
    }
    // test multiple rules on sufficient data -filter OK
    @Test
    public void e_evalMultipleRulesFilterOK() throws Exception{
        String uri = "/eval?filter=OK";

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.TEXT_PLAIN)
                .content(sufficientData)).andReturn();

        int status = result.getResponse().getStatus();
        String csv = result.getResponse().getContentAsString();
        assertEquals(200,status);
        assertEquals(result6,csv);
    }
    // test multiple rules on sufficient data -filter fail
    @Test
    public void f_evalMultipleRulesFilterFAIL() throws Exception{
        String uri = "/eval?filter=FAIL";

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.TEXT_PLAIN)
                .content(sufficientData)).andReturn();

        int status = result.getResponse().getStatus();
        String csv = result.getResponse().getContentAsString();
        assertEquals(200,status);
        assertEquals(result7,csv);
    }
    // test multiple rules on insufficient data
    @Test
    public void g_evalMultipleRulesOnInsufficientData() throws Exception{
        String uri = "/eval";

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.TEXT_PLAIN)
                .content(insufficientData1)).andReturn();

        int status = result.getResponse().getStatus();
        String csv = result.getResponse().getContentAsString();
        assertEquals(400,status);
        assertEquals(failure,csv);
    }



    private void fillDB() throws Exception {
        String uri = "/rules";
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonAlessB));
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonValLeqVal));
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonCNot3));
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonAnotA));
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json7neqD));
    }

    private void postPart1() throws Exception {
        String uri = "/rules";
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonAlessB));
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonValLeqVal));
    }

    private void postPart2() throws Exception {
        String uri = "/rules";
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonCNot3));
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonAnotA));
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json7neqD));
    }

    private void removeAllFromDB() throws Exception {
        String uri = "/rules";
        for(int i =1; i<=5;i++){
            mvc.perform(MockMvcRequestBuilders.delete(uri+"/"+i));
        }
    }

    private void removeContradiction() throws Exception {
        String uri = "/rules/9";
        mvc.perform(MockMvcRequestBuilders.delete(uri));
    }

    private void enable() throws Exception {
        String uri = "/rules/";
        mvc.perform(MockMvcRequestBuilders.put(uri+6)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonAlessB));
        mvc.perform(MockMvcRequestBuilders.put(uri+7)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonValLeqVal));
    }

    private void disable() throws Exception {
        String uri = "/rules/";
        mvc.perform(MockMvcRequestBuilders.put(uri+6)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonAlessBDissabled));
        mvc.perform(MockMvcRequestBuilders.put(uri+7)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonValLeqValDissabled));
    }

}
