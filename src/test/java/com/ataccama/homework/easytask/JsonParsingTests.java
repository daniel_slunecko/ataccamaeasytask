package com.ataccama.homework.easytask;

import com.ataccama.homework.easytask.rules.Rule;
import com.ataccama.homework.easytask.rules.arguments.RuleArgument;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JsonParsingTests {

    private String jsonAlessB;
    private String json7neqD;

    private String jsonAlessBwithId;
    private String json7neqDwithId;

    private Rule ruleAlessB;
    private Rule rule7neqD;

    private ObjectMapper oMapper;

    @Before
    public void init(){
        jsonAlessB = "{\"name\": \"AlessB\", \"enabled\": true, \"op\": \"<\", \"arg1\": {\"column\": \"A\"}, \"arg2\": {\"column\": \"B\"}}";
        json7neqD = "{\"name\": \"7neqD\", \"enabled\": false, \"op\": \"!=\", \"arg1\": {\"value\": 7}, \"arg2\": {\"column\": \"D\"}}";

        jsonAlessBwithId = "{\"id\":0,\"name\":\"AlessB\",\"enabled\":true,\"op\":\"<\",\"arg1\":{\"column\":\"A\"},\"arg2\":{\"column\":\"B\"}}";
        json7neqDwithId = "{\"id\":0,\"name\":\"7neqD\",\"enabled\":false,\"op\":\"!=\",\"arg1\":{\"value\":7},\"arg2\":{\"column\":\"D\"}}";

        RuleArgument ruleArgA = new RuleArgument(null, "A");
        RuleArgument ruleArgB = new RuleArgument(null, "B");
        RuleArgument ruleArgD = new RuleArgument(null, "D");
        RuleArgument ruleArg7 = new RuleArgument(7, null);

        ruleAlessB = new Rule("AlessB",true,"<", ruleArgA, ruleArgB);
        rule7neqD = new Rule("7neqD",false,"!=", ruleArg7, ruleArgD);

        oMapper = new ObjectMapper();
    }


    @Test
    public void ruleDeserializationCreatesCorrectInstances() throws IOException {
        ObjectReader ruleReader = oMapper.readerFor(Rule.class);

        Rule deserializedAlessB = ruleReader
                .readValue(jsonAlessB);
        Rule deserialized7neqD = ruleReader
                .readValue(json7neqD);

        assertEquals(deserializedAlessB,ruleAlessB);
        assertEquals(deserialized7neqD,rule7neqD);
    }

    @Test
    public void ruleSerializationCreatesCorrectJsons() throws JsonProcessingException {
        String serializedAlessB = oMapper.writeValueAsString(ruleAlessB);
        String serialized7neqD = oMapper.writeValueAsString(rule7neqD);

        assertEquals(jsonAlessBwithId,serializedAlessB);
        assertEquals(json7neqDwithId,serialized7neqD);

    }

}

