package com.ataccama.homework.easytask;

import com.ataccama.homework.easytask.rules.Rule;
import com.ataccama.homework.easytask.rules.arguments.RuleArgument;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;

/**
 *  Strongly inspired by:
 *  https://www.tutorialspoint.com/spring_boot/spring_boot_rest_controller_unit_test.htm
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class RuleCRUDTests extends AbstractRestTest {


    private String jsonAlessBwithId;
    private String json7neqDwithId;
    private String feasibleUpdateWithId;
    private String nonExistentUpdateWithId;
    private String infeasibleUpdateWithId;

    private Rule ruleAlessB;
    private Rule rule7neqD;

    @Override
    @Before
    public void setUp() {
        super.setUp();

        jsonAlessBwithId = "{\"id\":1,\"name\":\"AlessB\",\"enabled\":true,\"op\":\"<\",\"arg1\":{\"column\":\"A\"},\"arg2\":{\"column\":\"B\"}}";
        json7neqDwithId = "{\"id\":2,\"name\":\"7neqD\",\"enabled\":false,\"op\":\"!=\",\"arg1\":{\"value\":7},\"arg2\":{\"column\":\"D\"}}";
        feasibleUpdateWithId ="{\"id\":2,\"name\":\"7neqD\",\"enabled\":true,\"op\":\"!=\",\"arg1\":{\"value\":7},\"arg2\":{\"column\":\"D\"}}";
        nonExistentUpdateWithId = "{\"id\":42,\"name\":\"7neqD\",\"enabled\":true,\"op\":\"!=\",\"arg1\":{\"value\":7},\"arg2\":{\"column\":\"D\"}}";
        infeasibleUpdateWithId = "{\"id\":2,\"name\":\"3neqD\",\"enabled\":true,\"op\":\"!=\",\"arg1\":{\"value\":3},\"arg2\":{\"column\":\"D\"}}";


        RuleArgument ruleArgA = new RuleArgument(null, "A");
        RuleArgument ruleArgB = new RuleArgument(null, "B");
        RuleArgument ruleArgD = new RuleArgument(null, "D");
        RuleArgument ruleArg7 = new RuleArgument(7, null);

        ruleAlessB = new Rule("AlessB",true,"<", ruleArgA, ruleArgB);
        rule7neqD = new Rule("7neqD",false,"!=", ruleArg7, ruleArgD);

    }


    // creation test
    @Test
    public void createRules() throws Exception {
        String uri = "/rules";
        String ruleAlessBJson = super.mapToJson(ruleAlessB);
        String rule7neqDJson = super.mapToJson(rule7neqD);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(ruleAlessBJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(content, jsonAlessBwithId);

        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(rule7neqDJson)).andReturn();

        int status2 = mvcResult2.getResponse().getStatus();
        assertEquals(201, status2);
        String content2 = mvcResult2.getResponse().getContentAsString();
        assertEquals(content2, json7neqDwithId);
    }

    // readout tests

    // test reading of existing rule by id - code & response body
    @Test
    public void getRule() throws Exception {
        String uri = "/rules/1";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andReturn();

        int status = result.getResponse().getStatus();
        String json = result.getResponse().getContentAsString();

        assertEquals(status,200);
        assertEquals(json,jsonAlessBwithId);

    }

    // test reading of non-existing rule by id - code & message
    @Test
    public void getNonExistentRule() throws Exception {
        String uri = "/rules/42";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();

        int status = result.getResponse().getStatus();
        String message = result.getResponse().getContentAsString();

        assertEquals(status,404);
        assertEquals("A Rule with id: 42 doesn't exist",message);

    }

    // test reading of all existing rules - code, number of elements, correctness of elements
    @Test
    public void getAllRules() throws Exception {
        String uri = "/rules";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();

        int status = result.getResponse().getStatus();
        String json = result.getResponse().getContentAsString();
        String expected = String.format("[%s,%s]",jsonAlessBwithId,json7neqDwithId);

        assertEquals(status,200);
        assertEquals(expected,json);
    }

    // update tests

    //test feasible update - code and response body
    @Test
    public void updateRule() throws Exception {
        String uri = "/rules/2";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(feasibleUpdateWithId)).andReturn();

        int status = result.getResponse().getStatus();
        String json = result.getResponse().getContentAsString();

        assertEquals(status,200);
        assertEquals(feasibleUpdateWithId,json);
    }

    // test update of non-existent rule - code and message
    @Test
    public void updateNonExistingRule() throws Exception {
        String uri = "/rules/42";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(nonExistentUpdateWithId)).andReturn();

        int status = result.getResponse().getStatus();
        String json = result.getResponse().getContentAsString();
        String expected = "A Rule with id: 42 doesn't exist";

        assertEquals(status,404);
        assertEquals(expected,json);
    }

    // test update of non-updatable columns - code and message
    @Test
    public void updateNonUpdatableRuleColumn() throws Exception {
        String uri = "/rules/2";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(infeasibleUpdateWithId)).andReturn();

        int status = result.getResponse().getStatus();
        String json = result.getResponse().getContentAsString();
        String expected = "Illegal modification request. User is allowed to modify only name and/or state of enabled/disabled.";

        assertEquals(status,400);
        assertEquals(expected,json);
    }

    // delete tests
    // test feasible deletion request - code
    @Test
    public void deleteRule() throws Exception {
        String uri = "/rules/1";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();

        int status = result.getResponse().getStatus();
        assertEquals(204,status);
    }

    // test deletion of non-existent rule - code & response body
    @Test
    public void deleteNonExistentRule() throws Exception {
        String uri = "/rules/42";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();

        int status = result.getResponse().getStatus();
        String message = result.getResponse().getContentAsString();
        String expected = "A Rule with id: 42 doesn't exist";

        assertEquals(404,status);
        assertEquals(expected,message);
    }
}
